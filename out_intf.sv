interface out_intf();

    logic Pwm_Out;

    modport DUT(output Pwm_Out);

    modport MON(input  Pwm_Out);

endinterface