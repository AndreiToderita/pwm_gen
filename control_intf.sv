`include "control_intf_defines.svh"

interface control_intf(input Clk, Reset);

    logic CmdVal;
    logic R_WN;
    logic[`ADDR_WIDTH-1:0] Addr;
    logic[`DATA_WIDTH-1:0] Din;
    logic[`DATA_WIDTH-1:0] Dout;
    
    logic Enable;

    modport DUT(input CmdVal, R_WN, Addr, Din, Enable, Clk, Reset,
                output Dout);

    modport MON(input CmdVal, R_WN, Addr, Din, Enable, Clk, Reset, Dout);
endinterface