`include "tb_pwm_defines.svh"
module tb_pwm();

    reg tmp_Clk, tmp_Reset, tmp_Enable;

    control_intf control_intf_inst(.Clk(tmp_Clk),
                                   .Reset(tmp_Reset));
                                            
    out_intf out_intf_inst();

    PWM_WRAPPER PWM_WRAPPER_INST(.control_intf_inst(control_intf_inst),
                                 .out_intf_inst(out_intf_inst));

    test test_inst(.control_intf_inst(control_intf_inst),
                    .out_intf_inst(out_intf_inst));

task write_register(input integer wait_time,input [1:0] addres, input [15:0] data);
    repeat(wait_time)begin
        @(posedge tmp_Clk);
    end
        @(negedge tmp_Clk);
        control_intf_inst.CmdVal  <= 1'b1;
        control_intf_inst.R_WN    <= 1'b0;
        control_intf_inst.Addr    <= addres;
        control_intf_inst.Din     <= data;
        @(negedge tmp_Clk);
        control_intf_inst.CmdVal    <= 1'b0;
        control_intf_inst.R_WN      <= 1'b0;
        control_intf_inst.Addr      <= '0;
        control_intf_inst.Din       <= '0;
endtask

task read_register(input integer wait_time,input [1:0] addres);
    repeat(wait_time) begin
        @(posedge tmp_Clk);
    end
    @(negedge tmp_Clk);
    control_intf_inst.CmdVal <= 1'b1;
    control_intf_inst.R_WN   <= 1'b1;
    control_intf_inst.Addr   <= addres;
    @(negedge tmp_Clk);
    control_intf_inst.CmdVal <= 1'b0;
    control_intf_inst.R_WN    <= 1'b0;
    control_intf_inst.Addr   <= '0;
    control_intf_inst.Din    <= '0;
endtask

    always begin
        tmp_Clk = 1'b0;
        #`CLOCK_OFF;
        tmp_Clk = ~tmp_Clk;
        #`CLOCK_ON;
    end ;

    initial begin
        tmp_Clk = '0;
        control_intf_inst.Enable = 1'b1;   
        tmp_Reset = 1'b1;
        #10 tmp_Reset = 1'b0;

        write_register(10, `PRESCALER_REG, 16'h0002);

        read_register(40, `PRESCALER_REG);

        write_register(10, `DUTY_CYCLE_REG, 16'h0001 );
        write_register(10, `PERIOD_REG, 16'h0005);

        //am activat controlul
        write_register(5, `CONTROL_REG, 16'h0001);

        //vreau sa fac o modificare pe parcus a Perioadei
        write_register(10, `PERIOD_REG, 16'h000F);

        begin: dezactivare_Enable_SI_Reactivare
            repeat(10)begin
                @(posedge tmp_Clk);
            end
            control_intf_inst.Enable <= 1'b0;
            repeat(20)begin
                @(posedge tmp_Clk);
            end
            control_intf_inst.Enable <= 1'b1;
        end
        
        //Vreau sa fac un test de stres, voi pune Duty_Cycle > Perioada
        write_register(50, `DUTY_CYCLE_REG, 16'h0010);

        //Vreau sa readuc situatia la normal, setez, Period > Duty_Cycle
        write_register(700, `PERIOD_REG, 16'h0011);

        write_register(500, `DUTY_CYCLE_REG, 16'h0000);

        #20
            write_register(5, `DUTY_CYCLE_REG, 16'h0002);
            write_register(10, `PERIOD_REG, 16'h0005);

            write_register(2, `PRESCALER_REG, 16'h0001);

            #300 
            write_register(5, `PRESCALER_REG, 16'h0003);

            tmp_Reset = 1'b1;
            #100 tmp_Reset = 1'b0;

        #60
        write_register(10, `PRESCALER_REG, 16'h0002);
        write_register(10, `DUTY_CYCLE_REG, 16'h0001 );
        write_register(10, `PERIOD_REG, 16'h0005);

        //am activat controlul
        write_register(5, `CONTROL_REG, 16'h0001);
    end

    //voi realiza un always in care am scrieri si citiri succesive
    initial
        begin
            automatic int a = 0;
            #170;
            repeat(3) begin
                write_register(10, `DUTY_CYCLE_REG, a);

                read_register(20, `DUTY_CYCLE_REG);
                #30
                a++;
            end
        end
endmodule 