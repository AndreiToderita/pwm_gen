`include "PWM_TOP_defines.vh"

module PWM_TOP(input        CmdVal, //vom realiza semnalele de comanda, semnalele de intrare, semnalele de iesire
               input        R_WN,
               input[1:0]   Addr,
               input[15:0]  Din,
               output reg[15:0] Dout,
               
               input Clk,
               input Reset,
               input Enable,
               
               output Pwm_Out); 

//registrii interni ai DUT-ului
    reg[15:0]   duty_cycle_reg;
    reg[15:0]   period_reg;
    reg[2:0]    prescaler_reg;
    reg         control_reg;

//variabile si fire interne ale DUT-ului;
    integer count_prescaler     = 0;
    integer count_pwm_signal    = 0;
    reg[15:0] DutyCycle_buffer  = '0;
    reg[15:0] Period_buffer     = '0;
    reg[1:0]  Prescaler_buffer  = '0;
    reg     pwm_clk_intern      = 1'b0;
    reg     buffers_was_set     = 0;
    reg     count_is_actice     = 0;
    wire    clk_intern;

    assign clk_intern = control_reg == 1'b1 ? (Prescaler_buffer == 0 ? Clk : count_prescaler < ( 1'b1 << (Prescaler_buffer - 1)) ? 1'b1 : 1'b0): 
                                              1'b0; 

    assign Pwm_Out = Enable == 1'b1 ? pwm_clk_intern : 1'bz;

//functie pentru semnalarea faptului ca buffer-ele au fost setate
task assign_buffers;
    if((duty_cycle_reg < period_reg) && (duty_cycle_reg > 0))begin
                    Period_buffer       <= period_reg;
                    DutyCycle_buffer    <= duty_cycle_reg;
                    Prescaler_buffer    <= prescaler_reg;
                    count_is_actice     <= 1;
                    pwm_clk_intern      <= 1;
                    buffers_was_set     <= 1;
                end else if( duty_cycle_reg == 0) begin
                    count_is_actice <= 0;
                    pwm_clk_intern  <= 1'b0;
                    buffers_was_set <= 1'b0;
                end else if( duty_cycle_reg > period_reg)begin
                    count_is_actice <= 0;
                    pwm_clk_intern  <= 1'b1;
                    buffers_was_set <= 1'b0;
                end
endtask

//scrierea si citirea la registrii
    always@(posedge Clk)
        begin: setarea_si_configurarea_registrilor
             if(Reset)begin
                duty_cycle_reg  <=  '0;
                period_reg      <=  '0;
                prescaler_reg   <=  '0;
                control_reg     <=  '0;
                pwm_clk_intern  <=  '0;
                buffers_was_set <= '0;
                count_is_actice <= '0;
             end else begin
                 if( CmdVal == 1'b1 ) begin
                     case(Addr)
                        `DUTY_CYCLE_REG:  
                            if(R_WN)begin
                                Dout <= duty_cycle_reg;
                            end else begin
                                duty_cycle_reg <= Din;
                            end
                        `PERIOD_REG:  
                            if(R_WN)begin
                                Dout <= period_reg;
                            end else begin
                                period_reg <= Din;
                            end
                        `PRESCALER_REG:  
                            if(R_WN)begin
                                Dout <= prescaler_reg;              
                            end else begin
                                prescaler_reg <= Din;
                            end
                        `CONTROL_REG:  
                            if(R_WN)begin
                                Dout <= control_reg;
                            end else begin
                                control_reg <= Din;
                            end
                     endcase
                 end    
             end
        end

//generarte clock cu prescaler
always@(posedge Clk)
    begin : generare_clock_after_prescaler
        if(Reset)begin
            count_prescaler <= 0;
            Prescaler_buffer <= '0;
        end begin
            if(( count_prescaler < (1'b1 << Prescaler_buffer) - 1) && (control_reg == 1'b1)  )begin
                count_prescaler <= count_prescaler + 1;
            end else begin
                count_prescaler <= 0;
            end
        end
    end 

//generez semnalul PWM daca valoarea din registrii este ok
// Valoarea din registrii trebuie sa fie FU < PERIOD
always@(posedge clk_intern)
    begin: generare_semnal_PWM
        if(Reset) begin
            count_pwm_signal    <= 0;
            Period_buffer       <= 0;
            DutyCycle_buffer    <= 0;
            buffers_was_set     <= 0;
        end else begin
            if( !buffers_was_set && (!count_is_actice)) begin
                assign_buffers;
            end
            if(count_is_actice)begin
                if(count_pwm_signal < DutyCycle_buffer -1 )begin
                    pwm_clk_intern <= 1;
                end else if( count_pwm_signal < Period_buffer - 1 )begin
                    pwm_clk_intern <= 0;
                end 
                count_pwm_signal <= count_pwm_signal + 1;
                if(count_pwm_signal == Period_buffer -1 )begin
                    pwm_clk_intern      <= 1;
                    count_pwm_signal    <= 0;
                    assign_buffers;
                end 
            end
        end
    end
endmodule

