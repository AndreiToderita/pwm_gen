`include "timescale.vh"

module PWM_WRAPPER(control_intf.DUT control_intf_inst,
                   out_intf.DUT out_intf_inst);

        PWM_TOP PWM_TOP_INST(.CmdVal(control_intf_inst.CmdVal),
                             .R_WN(control_intf_inst.R_WN),
                             .Addr(control_intf_inst.Addr),
                             .Din(control_intf_inst.Din),
                             .Dout(control_intf_inst.Dout),
                             .Clk(control_intf_inst.Clk),
                             .Reset(control_intf_inst.Reset),
                             .Enable(control_intf_inst.Enable),
                             .Pwm_Out(out_intf_inst.Pwm_Out));
endmodule