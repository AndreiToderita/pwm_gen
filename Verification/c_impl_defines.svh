//`ifndef C_IMPL(ARG)
`define decl_c_impl(ARG) \
class c_impl_``ARG #(parameter type c_PktType, parameter type c_SbType) \
                     extends c_impl_intf #(c_PktType); \
    c_SbType Sb_h; \
    function new(input c_SbType Sb_h); \
        super.new();    \
        this.Sb_h = Sb_h;   \
    endfunction \
    function void write(input c_PktType Pkt); \
        this.Sb_h.write_``ARG(Pkt); \
    endfunction \
endclass \
//`endif