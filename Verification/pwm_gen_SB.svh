`ifndef __PWM_GEN_SB_SVH__
`define __PWM_GEN_SB_SVH__

`decl_c_impl(enable)
`decl_c_impl(reset)
`decl_c_impl(input_data)
`decl_c_impl(PwmOut)


class c_pwm_gen_SB;

    c_impl_reset#(.c_PktType(c_ResetPkt),
                  .c_SbType(c_pwm_gen_SB)) ResetImpl;

    c_impl_input_data#(.c_PktType(c_InputDataPkt),
                       .c_SbType(c_pwm_gen_SB)) InputImpl;

    c_impl_enable#(.c_PktType(c_EnPkt),
               .c_SbType(c_pwm_gen_SB)) EnableImpl;

    c_impl_PwmOut#(.c_PktType(output_pkt),
               .c_SbType(c_pwm_gen_SB)) PwmImpl;


    logic[15:0] DutyCycle_reg;
    logic[15:0] Period_reg;
    logic[2:0]  Prescaler_reg;
    logic       Control_reg;

    reset_state Pwm_reset_state;
    Pwm_Out_state Pwm_Out_state_inst;

    logic[15:0] DutyCycle_buffer;
    logic[15:0] Period_buffer;
    logic[2:0]  Prescaler_buffer;

    int DUT_Start_period_time;
    int DUT_DutyCycle_time;
    bit valid_pwm;

    extern function new();

    //extern task run();

    extern function void write_enable(c_EnPkt EnPkt);

    extern function void write_reset(c_ResetPkt ResetPkt);

    extern function void write_input_data(c_InputDataPkt InputDataPkt);

    //extern task collect_PwmOut_state(output_pkt output_pkt_inst);

    extern function void write_PwmOut(output_pkt output_pkt_inst);

endclass

    function c_pwm_gen_SB::new();
        ResetImpl = new(this);
        InputImpl = new(this);
        EnableImpl = new(this);
        PwmImpl = new(this);

        DutyCycle_reg = '0;
        Period_reg  = '0;
        Prescaler_reg = '0;
        Control_reg = 0;
        Pwm_Out_state_inst =  UNDEFINED_SIGNAL;
        Pwm_reset_state = RESET_DEASSERTED;
        DutyCycle_buffer = '0;
        Period_buffer = '0;
        Prescaler_buffer = '0;    
    endfunction

    function void c_pwm_gen_SB::write_enable(c_EnPkt EnPkt);
        c_EnPkt New_EnPkt;
        New_EnPkt = EnPkt.copy();
        //$display("@%0t PWM_GEN_SB : Am fost scoreboard", $time);
        if(New_EnPkt.enable_state_inst == ENABLE_ON)begin
            //$display("@%0t PWM_GEN_SB : Enable it's ON ", $time);
            Pwm_Out_state_inst = ACTIVE_STATE;
        end else begin
            //$display("@%0t PWM_GEN_SB :Enable_it's off ", $time);
            Pwm_Out_state_inst = HIGH_Z_STATE;
        end
    endfunction


    function void c_pwm_gen_SB::write_reset(c_ResetPkt ResetPkt);
        c_ResetPkt New_ResetPkt = ResetPkt.copy();

        if(ResetPkt.reset_state_inst == RESET_ASSERTED)begin
            DutyCycle_reg = '0;
            Period_reg = '0;
            Prescaler_reg = '0;
            Control_reg = '0;
            DutyCycle_buffer = '0;
            Period_buffer = '0;
            Prescaler_buffer = '0;
            //$display("@%0t PWM_GEN_SB : Reset Asserted ", $time);
            this.Pwm_reset_state = RESET_ASSERTED;
        end else begin
            //$display("@%0t PWM_GEN_SB : Reset DeAsserted ", $time);
            this.Pwm_reset_state = RESET_DEASSERTED;
        end
    endfunction

    function void c_pwm_gen_SB::write_input_data(c_InputDataPkt InputDataPkt);
        c_InputDataPkt  New_InputDataPkt =  InputDataPkt.copy();

        if(this.Pwm_reset_state == RESET_ASSERTED)begin
            DutyCycle_reg = '0;
            Period_reg = '0;
            Prescaler_reg = '0;
            Control_reg = '0;
            DutyCycle_buffer = '0;
            Period_buffer = '0;
            Prescaler_buffer = '0;
        end else begin
            //$display("@%0t PWM_GEN_SB : DEBUG : Registrii au fost accesati ", $time);
            case(New_InputDataPkt.Addr)
                `DUTY_CYCLE_REG : begin
                    if(New_InputDataPkt.COMMAND_OP_inst == WRITE)begin
                        DutyCycle_reg = New_InputDataPkt.Data;
                    end else if(New_InputDataPkt.COMMAND_OP_inst == READ)begin
                        if(DutyCycle_reg != New_InputDataPkt.Data)begin
                            $display("@%0t PWM_GEN_SB : Different read value on duty_Cycle ", $time);
                        end
                    end
                end
                `PERIOD_REG : begin
                    if(New_InputDataPkt.COMMAND_OP_inst == WRITE)begin
                        Period_reg = New_InputDataPkt.Data;
                    end else if(New_InputDataPkt.COMMAND_OP_inst == READ)begin
                        if(Period_reg != New_InputDataPkt.Data)begin
                            $display("@%0t PWM_GEN_SB : Different read value on Period_reg ", $time);
                        end
                    end
                end
                `PRESCALER_REG : begin
                    if(New_InputDataPkt.COMMAND_OP_inst == WRITE)begin
                        Prescaler_reg = New_InputDataPkt.Data;
                    end else if(New_InputDataPkt.COMMAND_OP_inst == READ)begin
                        if(Prescaler_reg != New_InputDataPkt.Data)begin
                            $display("@%0t PWM_GEN_SB : Different read value on Prescaler_reg ", $time);
                        end
                    end
                end
                `CONTROL_REG : begin
                    if(New_InputDataPkt.COMMAND_OP_inst == WRITE)begin
                        Control_reg = New_InputDataPkt.Data;
                    end else if(New_InputDataPkt.COMMAND_OP_inst == READ)begin
                        if(Control_reg != New_InputDataPkt.Data)begin
                            $display("@%0t PWM_GEN_SB : Different read value on Control_reg ", $time);
                        end
                    end
                end
            endcase
            if(Control_reg == 1'b0)begin
                DutyCycle_buffer = DutyCycle_reg;
                Period_buffer = Period_reg;
                Prescaler_buffer = Prescaler_reg;

                this.DUT_Start_period_time = 0;
                this.DUT_DutyCycle_time = 0;
            end
        end
    endfunction

    function void c_pwm_gen_SB::write_PwmOut(output_pkt output_pkt_inst);
        //$display("@%0t PWM_GEN_SB : Valorile de la iesire sunt procesate ", $time);
        if(output_pkt_inst.Pwm_Out_state_inst == HIGH_Z_STATE)begin
            this.DUT_Start_period_time = 0;
            this.DUT_DutyCycle_time = 0;
            if(this.Pwm_Out_state_inst != HIGH_Z_STATE )begin
                $display("@%0t PWM_GEN_SB : EROARE : PWM_MODEL_STATE(HIGH_Z) !=  PWM_DUT_STATE", $time);
            end
        end else if(output_pkt_inst.Pwm_Out_state_inst == ACTIVE_STATE)begin
           if(Control_reg == 1'b1) begin
                if(output_pkt_inst.Pwm_Out_logic_val_inst == ACTIVE_HIGH)begin
                    if(this.DUT_Start_period_time == 0)begin
                        this.DUT_Start_period_time = output_pkt_inst.signal_change_time;
                    end else begin
                        int model_period = (`CLOCK_ON + `CLOCK_OFF) * 2**Prescaler_buffer * Period_buffer ;
                        int model_duty_cycle = (`CLOCK_ON + `CLOCK_OFF) * 2**Prescaler_buffer * DutyCycle_buffer;

                        int DUT_Period = output_pkt_inst.signal_change_time - this.DUT_Start_period_time;
                        //$display("@%0t PWM_GEN_SB : DEBUG : It's working", $time);
                        if(model_period !=  DUT_Period)begin
                             $display("@%0t PWM_GEN_SB : EROARE : DUT_Period( %0d ns) != Model_Period(%0d ns)", $time, DUT_Period, model_period);
                        end
                        if(model_duty_cycle != this.DUT_DutyCycle_time)begin
                            $display("@%0t PWM_GEN_SB : EROARE : DUT_DutyCycle( %0d ns) != Model_DutyCycle(%0d ns)", $time, this.DUT_DutyCycle_time, model_duty_cycle);
                        end
                        this.DUT_Start_period_time = 0;
                        this.Prescaler_buffer = Prescaler_reg;
                        this.DutyCycle_buffer = DutyCycle_reg;
                        this.Period_buffer    = Period_reg;
                    end
                end else if(output_pkt_inst.Pwm_Out_logic_val_inst == ACTIVE_LOW)begin
                    this.DUT_DutyCycle_time = output_pkt_inst.signal_change_time - this.DUT_Start_period_time;
                end
           end 
        end else if(output_pkt_inst.Pwm_Out_state_inst == UNDEFINED_SIGNAL)begin
            $display("@%0t PWM_GEN_SB : EROARE : Valoarea semnalului este nedefinita", $time);
        end
    endfunction

    /*task pwm_gen_SB::collect_PwmOut_state(output_pkt output_pkt_inst);
            if(output_pkt_inst.Pwm_Out_state_inst != this.Pwm_Out_state_inst)begin
                    $display("@%0t PWM_GEN_SB: ERROR : Pwm_Out_state from DUT different from Model", $time);
            end
    endtask
    
    task pwm_gen_SB::collect_PwmOut(output_pkt output_pkt_inst);
            if(Control_reg == 1'b1)begin
                if(this.Pwm_Out_state_inst == ACTIVE_STATE)begin
                   int tmp_period;
                   int tmp_DutyCycle;
                   tmp_period = (`CLOCK_ON + `CLOCK_OFF) * 2**Prescaler_buffer * Period_buffer;
                   tmp_DutyCycle = (`CLOCK_ON + `CLOCK_OFF) * 2**Prescaler_buffer * DutyCycle_buffer;
                   //$display("@%0t PWM_GEN_SB: DEBUG : AM TESTAT", $time);
                   if(tmp_period != output_pkt_inst.Period_val)begin
                       $display("@%0t PWM_GEN_SB: EROARE : Pwm-ul are perioada diferita de model", $time);
                       $display("@%0t PWM_GEN_SB: DEBUG: PERIOD_DUT = %0d PERIOD_MODEL = %0d", $time, output_pkt_inst.Period_val, tmp_period);
                   end
                   if(tmp_DutyCycle != output_pkt_inst.DutyCycle_val )begin
                       $display("@%0t PWM_GEN_SB: EROARE : Pwm-ul are DutyCycle diferit de model", $time);
                       $display("@%0t PWM_GEN_SB: DEBUG: DUTY_CYCLE_DUT = %0d DUTY_CYCLE_MODEL = %0d", $time, output_pkt_inst.DutyCycle_val, tmp_DutyCycle);
                   end
                   this.Period_buffer = Period_reg;
                   this.DutyCycle_buffer = DutyCycle_reg;
                   this.Prescaler_buffer = Prescaler_reg;   
                end
            end else begin
                $display("@%0t PWM_GEN_SB : EROARE : Pwm-ul functioneaza cand Control_reg este 0", $time);
            end
    endtask*/
`endif