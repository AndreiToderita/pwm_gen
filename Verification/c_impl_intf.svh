`ifndef __C_IMPL_INTF_SVH__
`define __C_IMPL_INTF_SVH__

virtual class c_impl_intf#(parameter type c_PktType);

    extern function new();

    pure virtual function void write(c_PktType Pkt);

endclass

    function c_impl_intf::new();
    endfunction

`endif