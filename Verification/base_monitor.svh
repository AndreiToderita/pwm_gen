`ifndef __BASE_MONITOR_SVH__
`define __BASE_MONITOR_SVH__

virtual class base_mon#(parameter type vif_type,
                        parameter type c_SB_type);

    vif_type    vif_h;

    c_SB_type     qSbReset[$];
    c_SB_type     qSbEn[$];
    c_SB_type     qSbTr[$];

    extern function new();

    extern virtual function void connect_component(vif_type vif_h);

    extern task run();

    pure virtual task collect();

endclass

    function base_mon::new();
    endfunction

    function void base_mon::connect_component(vif_type vif_h);
        if(vif_h == null)begin
            $display(" ERROR => Bad_handle interface");
        end else begin
            this.vif_h = vif_h;
        end
        
    endfunction

    task base_mon::run();
        forever begin
            collect();
        end
    endtask

`endif