`ifndef __INPUT_MON_SVH__

class input_mon extends base_mon#(.vif_type(virtual control_intf.MON),
                                  .c_SB_type(c_pwm_gen_SB));

    c_EnPkt EnablePkt;
    c_ResetPkt  ResetPkt;
    c_InputDataPkt InputDataPkt;

    c_port#(c_ResetPkt) ResetPort;
    c_port#(c_EnPkt) EnablePort;
    c_port#(c_InputDataPkt) InputPort;

    extern function new();

    extern virtual task collect();
    
    //extern virtual function void writeEn(c_EnPkt EnablePkt);

    //extern virtual function void writeReset(c_ResetPkt  ResetPkt);

    //extern virtual function void writeData(c_InputDataPkt InputDataPkt);

    extern virtual function void display(COMMAND_OP COMMAND_OP_h, 
                                        logic[`ADDR_WIDTH-1:0] addr,
                                        logic[`DATA_WIDTH-1:0] data);
    
endclass

    function input_mon::new();
        ResetPort = new();
        InputPort = new();
        EnablePort = new();
    endfunction

    task input_mon::collect();
        fork
            forever 
                begin : collect_Enable
                    EnablePkt = new();
                    @(vif_h.Enable);
                    if(vif_h.Enable === 1'b1)begin    
                        $display("@%0t INPUT_MON : Enable_ON", $time);
                        EnablePkt.enable_state_inst = ENABLE_ON;
                    end else if(vif_h.Enable === 1'b0 )begin
                        $display("@%0t INPUT_MON : Enable_OFF", $time);
                        EnablePkt.enable_state_inst = ENABLE_OFF;
                    end else begin
                        $display("@%0t INPUT_MON : ERROR : Undefined Enable val", $time);
                    end
                    EnablePort.write(EnablePkt);
                end
            forever 
                begin : Collect_Reset
                    ResetPkt = new();
                    @(posedge vif_h.Clk);
                    if( vif_h.Reset === 1'b1 )begin
                        ResetPkt.reset_state_inst = RESET_ASSERTED;
                    end else if( vif_h.Reset === 1'b0 )begin
                        ResetPkt.reset_state_inst = RESET_DEASSERTED;
                    end
                    ResetPort.write(ResetPkt);
                end
            forever begin
                InputDataPkt = new();
                @(posedge vif_h.Clk);
                if( vif_h.Reset === 1'b0 )begin
                    if(vif_h.CmdVal === 1'b1)begin
                        casex(vif_h.R_WN)
                            1'b0: begin
                                if(vif_h.Addr !== {`ADDR_WIDTH{1'bx}} && vif_h.Din !=={`DATA_WIDTH{1'bx}})begin
                                    this.display(WRITE, vif_h.Addr, vif_h.Din );
                                    InputDataPkt.COMMAND_OP_inst = WRITE;
                                    InputDataPkt.Addr = vif_h.Addr;
                                    InputDataPkt.Data = vif_h.Din;
                                end else begin
                                    $display("@%0t: INPUT_MON: Signal unknown", $time);
                                end
                            end 
                            1'b1: begin
                                logic[`ADDR_WIDTH-1:0] addr_buffer;
                                if(vif_h.Addr!== {`ADDR_WIDTH{1'bx}})begin
                                    addr_buffer = vif_h.Addr;
                                end
                                @(negedge vif_h.Clk);
                                if(vif_h.Dout !=={`DATA_WIDTH{1'bx}})begin
                                    this.display(READ, addr_buffer, vif_h.Dout);
                                    InputDataPkt.COMMAND_OP_inst = READ;
                                    InputDataPkt.Data = vif_h.Dout;
                                end
                            end
                            default: begin
                                $display(" @%0t: INPUT_MON: Signal CmdVal it's unknown", $time);
                            end
                        endcase
                    end
                end else begin
                    $display("@%0t INPUT_MON : Reset Asserted", $time );
                    @(negedge vif_h.Reset);
                    $display("@%0t INPUT_MON : Reset Deasserted", $time);
                end
                InputPort.write(InputDataPkt);
            end
        join
    endtask

    /*function void input_mon::writeEn(c_EnPkt EnablePkt);
        foreach(qSbEn[count])
            begin
                qSbEn[count].collect_enable(EnablePkt);
            end
    endfunction*/

    /*function void input_mon::writeReset(c_ResetPkt ResetPkt);
        foreach(qSbReset[count])
            begin
                qSbReset[count].collect_reset(ResetPkt);
            end
    endfunction*/

    /*function void input_mon::writeData(c_InputDataPkt InputDataPkt);
        foreach(qSbTr[count])
            begin
                qSbTr[count].collect_input_data(InputDataPkt);
            end
    endfunction*/

    function void input_mon::display(COMMAND_OP COMMAND_OP_h, 
                                    logic[`ADDR_WIDTH-1:0] addr,
                                    logic[`DATA_WIDTH-1:0] data);

        $display("@%0t INPUT_MON : - Command = %s - Addres = 'h%0h - Data = 'h%0h", 
                $time, COMMAND_OP_h.name(), addr, data);
    endfunction
`endif