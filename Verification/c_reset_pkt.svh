`ifndef __C_RESET_PKT_SVH__
`define __C_RESET_PKT_SVH__

    class c_ResetPkt;
        reset_state reset_state_inst;

        extern function new();

        extern function c_ResetPkt copy();

    endclass

    function c_ResetPkt::new();
            this.reset_state_inst = RESET_DEASSERTED;
    endfunction

    function c_ResetPkt c_ResetPkt::copy();
            c_ResetPkt new_ResetPkt = new();

            new_ResetPkt.reset_state_inst = this.reset_state_inst;

            return new_ResetPkt;
    endfunction
`endif