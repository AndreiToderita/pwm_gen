`include "tb_pwm_defines.svh"

package verification_pkg;

    typedef enum{
        WRITE   = 0,
        READ    = 1,
        NO_OP   = 2
    }COMMAND_OP;

    typedef enum{
        ENABLE_OFF  = 0,
        ENABLE_ON   = 1
    }enable_state;

    typedef enum{
        RESET_ASSERTED      = 0,
        RESET_DEASSERTED    = 1
    }reset_state;

    typedef enum{
        ACTIVE_STATE        = 0,
        HIGH_Z_STATE        = 1,
        UNDEFINED_SIGNAL    = 2
    }Pwm_Out_state;
    typedef enum{
        ACTIVE_LOW = 0,
        ACTIVE_HIGH = 1
    }Pwm_Out_logic_val;

    
`include "input_pkt.svh"
`include "output_pkt.svh"

`include "c_enable_pkt.svh"
`include "c_reset_pkt.svh"
`include "c_input_data_pkt.svh"

`include "c_impl_intf.svh"
`include "c_port.svh"

//`include "c_impl_reset.svh"
//`include "c_impl_en.svh"
//`include "c_impl_tr.svh"
//`include "c_impl_pwm.svh"

`include "c_impl_defines.svh"

`include "pwm_gen_SB.svh"

`include "base_monitor.svh"
`include "input_mon.svh"
`include "output_mon.svh"

endpackage