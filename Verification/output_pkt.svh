`ifndef __OUTPUT_PKT_SVH__
`define __OUTPUT_PKT_SVH__

class output_pkt;

    Pwm_Out_state Pwm_Out_state_inst;
    Pwm_Out_logic_val Pwm_Out_logic_val_inst;
    //int DutyCycle_val;
    //int Period_val;

    int signal_change_time;

    extern function new();

    extern function output_pkt copy();

endclass

    function output_pkt::new();
        this.Pwm_Out_state_inst = UNDEFINED_SIGNAL;
        //this.DutyCycle_val = 0;
        //this.Period_val = 0;
        signal_change_time = 0;
    endfunction

    function output_pkt output_pkt::copy();
        output_pkt new_output_pkt = new();
        
        new_output_pkt.Pwm_Out_state_inst = this.Pwm_Out_state_inst;
        new_output_pkt.Pwm_Out_logic_val_inst  = this.Pwm_Out_logic_val_inst;
        new_output_pkt.signal_change_time = this.signal_change_time;
        //new_output_pkt.DutyCycle_val = this.DutyCycle_val;
        //new_output_pkt.Period_val = this.Period_val;

        return new_output_pkt;
    endfunction

`endif