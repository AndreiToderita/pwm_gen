`ifndef __C_IMPL_RESET_SVH
`define __C_IMPL_RESET_SVH

class c_impl_reset#(parameter type c_PktType,
                    parameter type c_SbType) extends c_impl_intf#(.c_PktType(c_PktType));
    c_SbType Sb_h;
    extern function new(c_SbType Sb_h);

    extern virtual function void write(c_PktType Pkt);

endclass

    function c_impl_reset::new(c_SbType Sb_h);
        super.new();
        this.Sb_h = Sb_h;
    endfunction

    function void c_impl_reset::write(c_PktType Pkt);
        this.Sb_h.collect_reset(Pkt);
    endfunction
`endif