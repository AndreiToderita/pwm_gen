`ifndef __OUTPUT_MON_SVH__
`define __OUTPUT_MON_SVH__

class output_mon extends base_mon#(.vif_type(virtual out_intf.MON),
                                    .c_SB_type(c_pwm_gen_SB));

    //int DutyCycle_buffer;
    //int Period_buffer;
    c_port#(output_pkt) PwmPort;

    extern function new();

    //extern function void writeTr(output_pkt output_pkt_h);

    extern virtual task collect();

endclass

    function output_mon::new();
        //DutyCycle_buffer = 0;
        //DutyCycle_buffer = 0;
        PwmPort = new();
    endfunction

    task output_mon::collect();
        output_pkt output_pkt_inst = new();
        @(vif_h.Pwm_Out);
        if(vif_h.Pwm_Out === 1'bz)begin
            output_pkt_inst.Pwm_Out_state_inst = HIGH_Z_STATE;
        end else if(vif_h.Pwm_Out === 1'bx)begin
            output_pkt_inst.Pwm_Out_state_inst = UNDEFINED_SIGNAL;
        end else begin
            if(vif_h.Pwm_Out == 1'b1)begin
                output_pkt_inst.Pwm_Out_state_inst = ACTIVE_STATE;
                output_pkt_inst.Pwm_Out_logic_val_inst = ACTIVE_HIGH;
            end else begin
                output_pkt_inst.Pwm_Out_state_inst = ACTIVE_STATE;
                output_pkt_inst.Pwm_Out_logic_val_inst = ACTIVE_LOW;
            end
            output_pkt_inst.signal_change_time = $time;
        end
        PwmPort.write(output_pkt_inst);
    endtask

    /*function void output_mon::writeTr(output_pkt output_pkt_h);
        foreach(this.qSbTr[count])begin
            this.qSbTr[count].collect_PwmOut(output_pkt_h);
        end 
    endfunction*/

    /*task output_mon::collect();
        int start_period = 0;
        int tmp_DutyCycle = 0;
        int tmp_Period = 0;

        DutyCycle_buffer = 0;
        Period_buffer = 0;
        fork
            forever begin
                output_pkt output_pkt_inst = new();
                @(posedge vif_h.Pwm_Out);
                if(vif_h.Pwm_Out === 1'bz) begin
                    start_period = 0;
                    tmp_DutyCycle = 0;
                    tmp_Period = 0;

                    DutyCycle_buffer = 0;
                    Period_buffer = 0;
                end else if(vif_h.Pwm_Out !== 1'bz) begin
                    output_pkt_inst.Pwm_Out_state_inst = ACTIVE_STATE;
                    if(start_period == 0) begin
                        start_period = $time;
                    end else begin
                        string display_str = "";
                        tmp_Period = $time - start_period;
                        if(tmp_DutyCycle != DutyCycle_buffer && (DutyCycle_buffer > 0)) begin
                        display_str = $sformatf("@%0t OUTPUT_MON : S-a schimbat semnalul Pwm_Out:", $time);
                            display_str = {display_str, $sformatf(" - DutyCycle < %0d ns > => < %0d ns >;",this.DutyCycle_buffer, tmp_DutyCycle)};
                            DutyCycle_buffer = tmp_DutyCycle;
                        end else if(DutyCycle_buffer == 0) begin
                            display_str = $sformatf("@%0t OUTPUT_MON : Semnalul Pwm_Out are o noi valori:", $time);
                            display_str = {display_str, $sformatf(" - DutyCycle = %0d ns;", tmp_DutyCycle)};
                            DutyCycle_buffer = tmp_DutyCycle;
                        end
                        if(tmp_Period != Period_buffer && (Period_buffer > 0))begin
                            display_str = {display_str, $sformatf( " - Perioada < %0d ns > => < %0d ns >;", this.Period_buffer, tmp_Period )};
                            Period_buffer = tmp_Period;
                            
                        end else if(Period_buffer == 0) begin
                            display_str = {display_str, $sformatf( " - Perioada  = %0d ns; ", tmp_Period )};
                            Period_buffer = tmp_Period;
                        end
                        if(display_str != "")begin
                            $display("%s", display_str);
                        end
                        output_pkt_inst.DutyCycle_val = tmp_DutyCycle;
                        output_pkt_inst.Period_val = tmp_Period;
                        SB_h.collect_PwmOut(output_pkt_inst.copy());
                    end
                    start_period = $time;
                    @(negedge vif_h.Pwm_Out);
                    tmp_DutyCycle = $time - start_period;
                end
            end 
            begin
                forever begin
                    @(posedge vif_h.Reset);
                    this.DutyCycle_buffer = 0;
                    this.Period_buffer = 0;
                    start_period = 0;
                    tmp_DutyCycle = 0;
                    tmp_Period = 0;
                end
            end
            begin
                forever begin
                    output_pkt output_pkt_inst = new();
                    @(vif_h.Pwm_Out);
                    if(vif_h.Pwm_Out === 1'bz) begin
                        $display("@%0t OUPUT_MON : Semnalul Pwm_Out a intrat in 'z;", $time);
                        output_pkt_inst.Pwm_Out_state_inst = HIGH_Z_STATE;
                    end else if(vif_h.Pwm_Out === 1'bx)begin
                        $display("@%0t OUPUT_MON : Semnalul Pwm_Out a intrat in 'x;", $time);
                        output_pkt_inst.Pwm_Out_state_inst = UNDEFINED_SIGNAL;
                    end else begin
                        output_pkt_inst.Pwm_Out_state_inst = ACTIVE_STATE;
                    end
                    SB_h.collect_PwmOut_state(output_pkt_inst.copy());
                    if(vif_h.Pwm_Out === 1'bz)begin
                        break;
                    end
                end
            end
        join_any
        disable fork;

    endtask*/
`endif  