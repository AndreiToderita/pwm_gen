import verification_pkg::*;

module test(control_intf control_intf_inst,
             out_intf  out_intf_inst);

    input_mon input_mon_h;
    output_mon output_mon_h; 
    c_pwm_gen_SB pwm_gen_SB_h;
    initial begin
        $timeformat(-9, 2, " ns", 20);

        //vom crea monitoarele
        input_mon_h = new();
        output_mon_h = new();
        pwm_gen_SB_h = new();

        //vom conecta monitoarele la interfete
        input_mon_h.connect_component(control_intf_inst);
        output_mon_h.connect_component(out_intf_inst);

        input_mon_h.ResetPort.connect(pwm_gen_SB_h.ResetImpl);
        input_mon_h.EnablePort.connect(pwm_gen_SB_h.EnableImpl);
        input_mon_h.InputPort.connect(pwm_gen_SB_h.InputImpl);

        output_mon_h.PwmPort.connect(pwm_gen_SB_h.PwmImpl);

        //vom starta monitoarele
        fork
            input_mon_h.run();
            output_mon_h.run();
        join
    end
endmodule