`ifndef __C_PORT_SVH__
`define __C_PORT_SVH__

class c_port#(parameter type c_PktType);

    c_impl_intf#(c_PktType) qImplIntf[$];

    extern function new();

    extern function void connect(c_impl_intf#(c_PktType) ImplIntf);

    extern function void write(c_PktType Pkt);

endclass

    function c_port::new();
    endfunction

    function void c_port::connect(c_impl_intf#(c_PktType) ImplIntf);
        if(ImplIntf != null)begin
            this.qImplIntf.push_front(ImplIntf);
        end else begin
            $display("@ %0t : PORT : ERROR : BAD handle", $time);
        end
    endfunction

    function void c_port::write(c_PktType Pkt);
        foreach(qImplIntf[count])begin
            qImplIntf[count].write(Pkt);
        end
    endfunction
`endif