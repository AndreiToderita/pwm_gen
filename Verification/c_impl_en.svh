`ifndef __C_IMPL_EN_SVH__
`define __C_IMPL_EN_SVH__

class c_impl_en#(parameter type c_PktType,
                 parameter type c_SbType) extends c_impl_intf#(.c_PktType(c_PktType));
    c_SbType Sb_h;
    
    extern function new(c_SbType Sb_h);

    extern virtual function void write(c_PktType Pkt);

endclass

    function c_impl_en::new(c_SbType Sb_h);
        super.new();
        this.Sb_h = Sb_h;
    endfunction

    function void c_impl_en::write(c_PktType Pkt);
        this.Sb_h.collect_enable(Pkt);
    endfunction

`endif