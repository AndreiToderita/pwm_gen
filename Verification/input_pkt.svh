`ifndef __INPUT_PKT_SVH__
`define __INTPUT_PKT_SVH__

    class input_pkt;

        COMMAND_OP COMMAND_OP_inst;
        enable_state enable_state_inst;
        reset_state reset_state_inst;
        logic[`ADDR_WIDTH-1:0] Addr;
        logic[`DATA_WIDTH-1:0] Data;

        extern function new();

        extern function input_pkt copy();
    endclass

        function input_pkt::new();
            this.COMMAND_OP_inst = NO_OP;
            this.enable_state_inst = ENABLE_OFF;
            this.reset_state_inst = RESET_DEASSERTED;
            this.Addr = 'x;
            this.Data = 'x;
        endfunction

        function input_pkt input_pkt::copy();
            input_pkt new_input_p = new();

            new_input_p.COMMAND_OP_inst = this.COMMAND_OP_inst;
            new_input_p.enable_state_inst = this.enable_state_inst;
            new_input_p.reset_state_inst = this.reset_state_inst;
            new_input_p.Addr = this.Addr;
            new_input_p.Data = this.Data;

            return new_input_p; 
        endfunction

`endif