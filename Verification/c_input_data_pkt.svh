`ifndef __C_INPUT_DATA_PKT_SVH__
`define __C_INPUT_DATA_PKT_SVH__


    class c_InputDataPkt;

        COMMAND_OP COMMAND_OP_inst;
        logic[`ADDR_WIDTH-1:0] Addr;
        logic[`DATA_WIDTH-1:0] Data;

        extern function new();

        extern function c_InputDataPkt copy();
    endclass

     function c_InputDataPkt::new();
        this.COMMAND_OP_inst = NO_OP;
        this.Addr = 'x;
        this.Data = 'x;
    endfunction

    function c_InputDataPkt c_InputDataPkt::copy();
        c_InputDataPkt InputDataPkt = new();

        InputDataPkt.COMMAND_OP_inst = this.COMMAND_OP_inst;
        InputDataPkt.Addr = this.Addr;
        InputDataPkt.Data = this.Data;

        return InputDataPkt; 
    endfunction
`endif