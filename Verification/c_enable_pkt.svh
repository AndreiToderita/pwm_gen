`ifndef __C_ENABLE_PKT_SVH__
`define __C_ENABLE_PKT_SVH__

    class c_EnPkt;
        enable_state enable_state_inst;

        extern function new();

        extern function c_EnPkt copy();
    
    endclass
    
    function c_EnPkt::new();
            this.enable_state_inst = ENABLE_OFF;
    endfunction

    function c_EnPkt c_EnPkt::copy();
            c_EnPkt new_EnPkt = new();
            new_EnPkt.enable_state_inst = this.enable_state_inst;
            return new_EnPkt; 
        endfunction
`endif
