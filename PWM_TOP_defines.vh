`ifndef __PWM_TOP_DEFINES_VH__
`define __PWM_TOP_DEFINES_VH__

    `include "timescale.vh"

    `define DUTY_CYCLE_REG  2'b00
    `define PERIOD_REG      2'b01
    `define PRESCALER_REG   2'b10
    `define CONTROL_REG     2'b11

`endif